import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController, NavController, Platform, ToastController } from '@ionic/angular';
import { delay, retryWhen, take } from 'rxjs/operators';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { environment } from 'src/environments/environment';
import { LocalFactory } from 'src/helpers/storage.helper';
import { DeviceStatus } from '../models/device_status.model';
import { AuthenticationService } from '../services/authentication.service';
import { CustomerService } from '../services/customer.service';
import { SocketService } from '../services/socket.service';
// interface CurrentState {
//   V1: boolean
//   V2: boolean
//   L1: boolean
//   L2: boolean
// }

interface CurrentState {
  V1: number
  V2: number
  L1: number
  L2: number
}

@Component({
  selector: 'app-device-status',
  templateUrl: './device-status.page.html',
  styleUrls: ['./device-status.page.scss'],
})
export class DeviceStatusPage implements OnInit, OnDestroy {

  deviceStatus: DeviceStatus = <DeviceStatus>{}
  deviceID
  currentState = <CurrentState>{
    V1: 0,
    V2: 0,
    L1: 0,
    L2: 0,
  }
  ws: WebSocketSubject<any>
  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private socket: SocketService,
    private customerService: CustomerService,
    private toastCtrl: ToastController,
    public actionSheetController: ActionSheetController,
    public authService: AuthenticationService,
    private navCtrl: NavController,
    private platform: Platform,
  ) {
    this.socket.connect(`${environment.baseWS}?device_id`)
  }
  device
  filterSetting = LocalFactory.getItem('filterSetting') || defaultFilterSetting
  ngOnInit() {
    var params = this.route.snapshot.queryParams
    this.deviceID = params['device_id']
    this.isOffline = params['is_offline']
    this.connectWs(params['device_id'])
    console.log(this.deviceID, this.isOffline)
    this.getDeviceSetting('SETUP_RECOVER_COLDWATER')
    // this.getValveSetting()
    this.getBuNhietSetting()
    this.getHoiNuocSetting()
    this.lockBack()
  }

  lockBackSubscription
  lockBack() {
    console.log('lockback')
    this.lockBackSubscription = this.platform.backButton.observers.pop()
  }


  getDeviceDetail() {
    this.customerService.getDeviceDetail(this.deviceID).subscribe(dev => {
      this.device = dev
    })
  }

  isOnline() {
    return (new Date().getTime() / 1000 - this.device['last_online'])
  }
  loadSetting() {
    this.getDeviceSetting('SETUP_RECOVER_COLDWATER')
    // this.getValveSetting()
    this.getBuNhietSetting()
    this.getHoiNuocSetting()
  }

  deviceSetting = <any>{}
  valveSetting = <any>{}
  getDeviceSetting(command) {
    this.customerService.getSetting(this.deviceID, command).subscribe(setting => {
      if (setting) {
        this.deviceSetting = setting
      } else {
        this.deviceSetting = defaultDeviceStatus
      }
    })
  }

  getValveSetting() {
    this.customerService.getSetting(this.deviceID, 'VALVE').subscribe(setting => {
      if (setting) {
        this.valveSetting = setting
      } else {
        this.valveSetting = defaultValveDeviceStatus
      }
    })
  }

  buNhietSetting = <any>{}
  getBuNhietSetting() {
    this.customerService.getSetting(this.deviceID, 'SETUP_COMP_HEATER').subscribe(setting => {
      if (setting) {
        this.buNhietSetting = setting
      } else {
        this.buNhietSetting = defaultBuNhietStatus
      }
    })
  }

  hoiNuocSetting = <any>{}
  getHoiNuocSetting() {
    this.customerService.getSetting(this.deviceID, 'RECOVER_COLDWATER').subscribe(setting => {
      if (setting) {
        this.hoiNuocSetting = setting
      } else {
        this.hoiNuocSetting = defaultHoiNuocStatus
      }
    })
  }

  async presentActionSheet() {
    if (this.isOffline) {
      this.showToast('Thiết bị mất kết nối. Vui lòng kiểm tra lại!')
      return
    }
    const actionSheet = await this.actionSheetController.create({
      header: 'Chế độ tuần hoàn',
      cssClass: 'my-custom-class',
      buttons: [
        {
          text: 'Tự động',
          icon: 'aperture',
          handler: () => {
            console.log('Share clicked');
            this.switchServer(2)
          }
        }, {
          text: 'Biểu lập và cưỡng bức',
          icon: 'calendar',
          handler: () => {
            this.switchServer(3)
          }
        }, {
          text: 'Cưỡng bức',
          icon: 'cube',
          handler: () => {
            this.switchServer(4)
          }
        },
        {
          text: 'Switch Box',
          role: 'selected',
          icon: 'git-compare',
          handler: () => {
            this.switchServer(0)
          }
        }, {
          text: 'Tắt hệ thống',
          role: 'selected',
          icon: 'power',
          handler: () => {
            this.switchServer(1)
          }
        }, {
          text: 'Đóng',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async presentForceActionSheet() {
    console.log(this.isOffline)
    if (this.isOffline) {
      this.showToast('Thiết bị mất kết nối. Vui lòng kiểm tra lại!')
      return
    }
    const actionSheet = await this.actionSheetController.create({
      header: 'Chọn phân vùng',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Phân vùng 1',
        role: 'selected',
        icon: 'aperture',
        handler: () => {
          this.force(1)
        }
      }, {
        text: 'Phân vùng 2',
        icon: 'aperture',
        handler: () => {
          this.force(2)
        }
      }, {
        text: 'Đóng',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  force(van) {
    console.log('r3', this.deviceStatus['R3'])
    console.log('S3', this.hoiNuocSetting['S33'])
    console.log('D3', this.hoiNuocSetting['D33'])
    if (this.deviceStatus['R3'] > (this.hoiNuocSetting['S33'] + this.hoiNuocSetting['D33'])) {
      if (this.deviceStatus['R' + van] <= (this.hoiNuocSetting['S' + van] - this.hoiNuocSetting['D' + van])) {
        this.showToast('Bạn vui lòng chờ đợi trong ít phút!')
      } else {
        this.showToast('Đã hoàn tất bạn nhé!')
      }
    }
    console.log('this.deviceStatus[R3] < (this.hoiNuocSetting[S3] - this.hoiNuocSetting[D3])', this.deviceStatus['R3'], (this.hoiNuocSetting['S3'], this.hoiNuocSetting['D3']))

    if (this.deviceStatus['R3'] < (this.hoiNuocSetting['S33'] - this.hoiNuocSetting['D33'])) {
      if (this.deviceStatus['R' + van] <= (this.hoiNuocSetting['S' + van] - this.hoiNuocSetting['D' + van])) {
        this.showToast('Nhiệt độ nước trong bình thấp')
      }
    }


    // if (this.deviceStatus['R3'] < (this.hoiNuocSetting.S1)) {
    //   this.showToast('Nhiệt độ nước trong bình thấp!')
    // }
    this.deviceSetting.Command = 'FORCED'
    this.deviceSetting.ID = this.deviceID
    this.deviceSetting.VAN = van
    this.customerService.commandToDevice(this.deviceSetting).subscribe(_ => {
      // this.showToast('Cài đặt thành công!')
    })
  }
  checkOnline() {
  }
  switchServer(st) {
    if (this.isOffline) {
      this.showToast('Thiết bị mất kết nối. Vui lòng kiểm tra lại!')
      return
    }
    this.deviceSetting.Command = 'MODE'
    this.deviceSetting.ST = st
    this.deviceSetting.ID = this.deviceID
    this.customerService.commandToDevice(this.deviceSetting).subscribe(_ => {
      this.showToast('Cài đặt thành công!')
    })
  }


  openValveByPass() {
    this.valveSetting.Command = 'VALVE'
    this.valveSetting.ST = (this.valveSetting.ST == 1 ? 0 : 1)
    this.valveSetting.ID = this.deviceID
    this.customerService.commandToDevice(this.valveSetting).subscribe(_ => {
      this.toastCtrl.create({ message: 'Cài đặt thành công!' })
    })
  }

  buNhietCuongBuc() {
    if (this.isOffline) {
      this.showToast('Thiết bị mất kết nối. Vui lòng kiểm tra lại!')
      return
    }
    this.buNhietSetting.Command = 'SETUP_COMP_HEATER'
    this.buNhietSetting.ST = (this.buNhietSetting.ST == 1 ? 0 : 1)
    this.buNhietSetting.ID = this.deviceID
    this.customerService.commandToDevice(this.buNhietSetting).subscribe(_ => {
      this.toastCtrl.create({ message: 'Cài đặt thành công!' })
    })
  }

  ngOnDestroy(): void {
    if (this.ws) {
      this.ws.unsubscribe()
    }
  }

  onControl() {
    this.deviceStatus.Command = "CONTROL"
    this.deviceSetting.ID = this.deviceID
    this.setDefaultTime()
    this.customerService.controlDevice(this.deviceStatus).subscribe(_ => {
      this.showToast('Cập nhật trạng thái thiết bị thành công')
    })
  }
  isOffline = false
  connectWs(deviceID) {
    this.ws = webSocket(`${environment.baseWS}?device_id=${deviceID}`);
    this.ws.pipe(
      retryWhen((errors) => errors.pipe(delay(3000), take(100)))
    ).subscribe(res => {
      this.loadSetting()
      if (res) {
        this.isOffline = false
        this.deviceStatus = <DeviceStatus>res
        if (this.nhietToiTe(res)) {
          this.showToast('Nhiệt tồi tệ!')
        }
        this.authService.currentGroup
        if (['RKRB4q7WGo6aHeE2cYIq5bO7', 'W1Agqq2bhpGXbaL1ScROrzhD'].includes(this.authService.currentGroup)) {
          if (this.khongBuNhiet(res)) {
            this.showToast('Không bù nhiệt!')
          }
        }

        if (this.mucNuocThap()) {
          this.showToast('Mực nước thấp!')
        }
        this.caculateWL(status)
        // this.setCurrentState()
      } else {
        this.isOffline = true
      }
    })
  }
  WL = -1
  caculateWL(status) {
    if (status.WL == 32678) {
      this.WL = -1
    } else {
      if (status.HW != 0) {
        this.WL = (status.HW - status.WL) / status.HW
      }
    }
  }

  mucNuocThap() {
    if (this.WL >= 0 && this.WL <= this.filterSetting.LW) {
      return true
    }
    return false
  }

  nhietToiTe(status) {
    if (status.R3 <= this.filterSetting.S36) {
      return true
    }
    return false
  }

  khongBuNhiet(status) {
    if (status.R3 <= this.filterSetting.S35) {
      return true
    }
    return false
  }

  setCurrentState() {
    // this.currentState.V1 = this.deviceStatus.V1 == 1 ? true : false
    // this.currentState.V2 = this.deviceStatus.V2 == 1 ? true : false
    // this.currentState.L1 = this.deviceStatus.L1 == 1 ? true : false
    // this.currentState.L2 = this.deviceStatus.L2 == 1 ? true : false

    this.currentState.V1 = this.deviceStatus.V1
    this.currentState.V2 = this.deviceStatus.V2
  }

  onBack() {
    this.navCtrl.back()
  }

  ionViewDidLeave() {
    if (this.ws) {
      this.ws.unsubscribe()
    }
  }

  setDefaultTime() {
    this.deviceStatus.Time = {
      Hours: 0,
      Minutes: 0
    }
  }

  onRemote(actor: string, currentState: number) {
    currentState = currentState == 1 ? 0 : 1
  }

  async showToast(msg: string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }


}

const defaultDeviceStatus = <any>{
  S1: 42,
  S2: 42,
  S33: 38,
  D1: 1.6,
  D2: 1.6,
  D33: 0.8,
  C1: 2,
  C2: 2,
  C3: 2,
  ST: 0,
}

const defaultValveDeviceStatus = <any>{
  ST: 0,
}

const defaultBuNhietStatus = <any>{
  D38: 1.3,
  S38: 46.6,
  ST: 0,
}

const defaultHoiNuocStatus = <any>{
  S1: 42,
  S2: 42,
  S33: 38,
  D1: 1.6,
  D2: 1.6,
  D33: 0.8,
  C1: 2,
  C2: 2,
  C3: 2,
  ST: 0,
}

const defaultFilterSetting = {
  D35: 1,
  S35: 48,
  S36: 40,
  D36: 1,
  LW: 50,
  MD: 180,
}