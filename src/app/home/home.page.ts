import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, AlertController, NavController, ToastController } from '@ionic/angular';
import { interval, timer } from 'rxjs';
import { delay, retryWhen, take } from 'rxjs/operators';
import { webSocket } from 'rxjs/webSocket';
import { environment } from 'src/environments/environment';
import { DeviceStatus } from '../models/device_status.model';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage {

  deviceStatus = <DeviceStatus>{}
  devices = []
  constructor(
    private customerService: CustomerService,
    private navCtrl: NavController,
    private router: Router,
    private toastCtrl: ToastController,
    public actionSheetController: ActionSheetController,
    private alertCtrl:AlertController
  ) { }

  timerSubscription

  ionViewDidEnter() {
    this.timerSubscription = timer(0, 5000).subscribe(_ => {
      this.loadDevices()
    })
    this.loadDeviceStatus()
  }

  ionViewWillLeave() {
    console.log('view will leave');
  }

  ionViewDidLeave() {
    this.timerSubscription.unsubscribe()
  }


  ngOnDestroy() {
    this.timerSubscription = timer(0, 5000).subscribe(_ => {
      this.loadDevices()
    })
    this.loadDeviceStatus()
  }

  loadDeviceStatus() {

  }

  loadDevices() {
    this.customerService.getDevices().subscribe(devices => {
      this.devices = devices || []
    })
  }

  async onLogout() {
    var alert  = await this.alertCtrl.create({
      header: 'Đăng xuất',
      subHeader: 'Bạn có chắc muốn đăng xuất',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.router.navigate(['/login'])
          }
        },
        {
          text: 'Huỷ',
          handler: () => {
            console.log('Let me think');
          }
        },
      ]
      
    })
    await alert.present();
    // alert.onWillDismiss.
      // this.router.navigate(['/login'])
  }

  onRemote(actor: string) {

  }
  isOffline(device) {
    return (new Date().getTime() / 1000 - device.last_online) > 60
  }

  async showToast(msg: string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

  onNavigateToDeviceStatus(device) {
    // if (this.isOffline(device)) {
    //   this.showToast('Thiết bị mất kết nối. Vui lòng kiểm tra lại thiết bị')
    //   return
    // }
    this.navCtrl.navigateForward(['/tabs/device-status'], {
      queryParams: {
        device_id: device['mac'],
        is_offline:this.isOffline(device),
      }
    })
    // this.router.navigate(['device-status'], {
    //   state: {
    //     device_id: device['mac']
    //   },
    //   queryParams: {
    //     device_id: device['mac']
    //   },
    // })
  }

  async showActionSheet(device) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Chọn chức năng',
      buttons: [{
        text: 'Giám sát',
        icon: 'bar-chart-outline',
        handler: () => {
          this.onNavigateToDeviceStatus(device)
        }
      }, {
        text: 'Lập biểu',
        icon: 'calendar-outline',
        handler: () => {
          this.navCtrl.navigateForward(['/tabs/time-frame'], {
            queryParams: {
              device_id: device['mac']
            }
          })
        }
      }, {
        text: 'Hủy',
        icon: 'close',
        role: 'cancel',
        handler: () => {
        }
      }]
    });
    await actionSheet.present();

    const { role, data } = await actionSheet.onDidDismiss();
  }

  trackByStatus(index, item){
    return item.last_online; 
 }
}
