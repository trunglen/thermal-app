import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { LocalFactory } from 'src/helpers/storage.helper';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {

  constructor(
    private toastCtrl: ToastController,
  ) { }
  filterSetting = LocalFactory.getItem('filterSetting') || defaultFilterSetting
  ngOnInit() {
  }

  onSetFilter() {
    LocalFactory.setItem('filterSetting', this.filterSetting)
    this.showToast('Đã lưu cài đặt')
  }

  async showToast(msg: string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}

const defaultFilterSetting = {
  D35: 1,
  S35: 48,
  S36: 40,
  D36: 1,
  LW: 50,
  MD: 180,
}