import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimeFramePageRoutingModule } from './time-frame-routing.module';

import { TimeFramePage } from './time-frame.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimeFramePageRoutingModule,
    FormsModule
  ],
  declarations: [TimeFramePage]
})
export class TimeFramePageModule {}
