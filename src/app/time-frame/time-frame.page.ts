import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { CustomerService } from '../services/customer.service';
import { format, parseISO } from 'date-fns';

@Component({
  selector: 'app-time-frame',
  templateUrl: './time-frame.page.html',
  styleUrls: ['./time-frame.page.scss'],
})
export class TimeFramePage implements OnInit {

  constructor(
    private customerService: CustomerService,
    private activatedRoute: ActivatedRoute,
    private toastCtrl: ToastController,
    private navCtrl: NavController,
    private platform: Platform,
  ) { }

  tfs = defaultTfs
  tfsLocalSetting = {}
  deviceID
  mode = 'mode1'
  ngOnInit() {
    const dateFromIonDatetime = '2022-01-26T09:00:09.425+07:00';
    const formattedString = format(parseISO(dateFromIonDatetime), 'hh:mm');
    console.log(formattedString)
    this.deviceID = this.activatedRoute.snapshot.queryParams['device_id']
    console.log(this.deviceID)
    this.mode = localStorage.getItem('tf_setting_mode_v2') || 'custom'
    var tfs = localStorage.getItem('tf_setting_v2')
    if (tfs) {
      var temp = JSON.parse(tfs)
      this.tfsLocalSetting = temp

      if (temp[this.mode]) {
        this.tfs = temp[this.mode]
      }
      console.log(this.tfs)
    }
    this.lockBack()
  }
  ionViewDidEnter() {
    this.lockBack()
  }
  lockBackSubscription
  lockBack() {
    console.log('lockback')
    this.platform.backButton.subscribe(()=>{
      console.log(' this.platform.backButton')
      return
    })
    document.addEventListener('backbutton', async function (event) {
      console.log( ' document.addEventListener(backbutton)')
      event.stopImmediatePropagation();
      event.preventDefault();
      event.stopPropagation();
      return;
      //continue event
    }, false);
    this.lockBackSubscription = this.platform.backButton.observers.pop()
  }

  onSetupTimeframe() {
    console.log(this.tfs)
    var body = {}
    body['device_mac'] = this.deviceID
    body['time_ranges'] = []
    for (let index = 0; index < this.tfs.length; index++) {
      const tf = this.tfs[index];
      if (tf.from != '' && tf.to != '') {
        if (tf.from.length != 5 && tf.to.length != 5) {
          tf.from = format(parseISO(tf.from), 'hh:mm');
          tf.to = format(parseISO(tf.to), 'hh:mm');
          this.tfs[index].from = tf.from
          this.tfs[index].to = tf.to
        }

        body['time_ranges'].push({
          TSH: Number.parseInt(tf.from.split(':')[0]),
          TSM: Number.parseInt(tf.from.split(':')[1]),
          TFH: Number.parseInt(tf.to.split(':')[0]),
          TFM: Number.parseInt(tf.to.split(':')[1]),
        })
      }
    }
    var tfSetting = this.tfsLocalSetting || {}
    tfSetting[this.mode] = this.tfs
    localStorage.setItem('tf_setting_v2', JSON.stringify(tfSetting))
    localStorage.setItem('tf_setting_mode_v2', this.mode)
    // this.showToast('Cài đặt thành công')
    this.customerService.setupDeviceTimeFrame(body).subscribe(_ => {
      this.showToast('Cài đặt thành công')
    })
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

  onChangeMode(mode) {
    // this.tfs = defaultTfs
    console.log(this.tfsLocalSetting)
    this.tfs = defaultTfs

    switch (mode) {
      case 'mode1':
        if (this.tfsLocalSetting && this.tfsLocalSetting['mode11']) {
          // this.tfs = this.tfsLocalSetting['mode1']
          this.tfs[0].from = '04:00'
          this.tfs[0].to = '24:00'
          console.log(this.tfs)
        } else {
          this.tfs[0].from = '04:00'
          this.tfs[0].to = '24:00'
        }

        break;
      case 'mode2':
        if (this.tfsLocalSetting && this.tfsLocalSetting['mode21']) {
          // this.tfs = this.tfsLocalSetting['mode2']
          this.tfs[0].from = '05:00'
          this.tfs[0].to = '08:00'
          this.tfs[1].from = '16:00'
          this.tfs[1].to = '21:00'
        } else {
          this.tfs[0].from = '05:00'
          this.tfs[0].to = '08:00'
          this.tfs[1].from = '16:00'
          this.tfs[1].to = '21:00'
        }

        break
      case 'mode3':
        if (this.tfsLocalSetting && this.tfsLocalSetting['mode31']) {
          // this.tfs = this.tfsLocalSetting['mode3']
          this.tfs = [
            { from: '05:00', to: '08:00' },
            { from: '08:50', to: '09:10' },
            { from: '09:50', to: '10:10' },
            { from: '10:50', to: '13:00' },
            { from: '13:50', to: '14:10' },
            { from: '14:50', to: '15:10' },
            { from: '15:50', to: '21:00' },
            { from: '21:50', to: '22:10' },
            { from: '22:50', to: '00:00' },
          ]
        } else {
          this.tfs = [
            { from: '05:00', to: '08:00' },
            { from: '08:50', to: '09:10' },
            { from: '09:50', to: '10:10' },
            { from: '10:50', to: '13:00' },
            { from: '13:50', to: '14:10' },
            { from: '14:50', to: '15:10' },
            { from: '15:50', to: '21:00' },
            { from: '21:50', to: '22:10' },
            { from: '22:50', to: '00:00' },
          ]
        }
        break
      default:
        if (this.tfsLocalSetting && this.tfsLocalSetting['custom']) {
          this.tfs = this.tfsLocalSetting['custom']
        } else {
          this.tfs = defaultTfs
          for (let index = 0; index < defaultTfs.length; index++) {
            this.tfs[index].from = ''
            this.tfs[index].to = ''
          }
        }

        console.log(this.tfs)
        break;
    }

  }

  onBack() {
    this.navCtrl.back()
  }


}


const defaultTfs = [
  { from: '', to: '' },
  { from: '', to: '' },
  { from: '', to: '' },
  { from: '', to: '' },
  { from: '', to: '' },
  { from: '', to: '' },
  { from: '', to: '' },
  { from: '', to: '' },
  { from: '', to: '' },
]