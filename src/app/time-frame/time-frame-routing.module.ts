import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimeFramePage } from './time-frame.page';

const routes: Routes = [
  {
    path: '',
    component: TimeFramePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimeFramePageRoutingModule {}
